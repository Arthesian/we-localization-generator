# WE Localization Generator

This web application's purpose is to generate localizations for Wallpaper Engine projects.

It supports auto translating existing properties to new languages as well ( powered by Google Translate )