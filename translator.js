var Translator = function () {

    // Scope object
    var _ = {};

    // Private variables
    var url = "https://script.google.com/macros/s/AKfycbwIOgR9xp24rtp8ZhlgFqGHxyw2WGvZc7ZXO29NI-l7K-2M3Phx/exec";;

    // Public variables
    _.sourceLang = 'auto';
    _.targetLang = 'en';
    _.searchText = '';

    /**
     * @private
     * 
     * Initialization function
     */
    var init = function () {

    }
    /**
     * Set the source language of the text to be translated
     * 
     * @param {any} code 2-Letter code for the language ( ex. 'NL' )
     */
    _.setSourceLanguage = function (code) {
        _.srouceLang = code;
    }

    /**
     * Set the target language of the text to be translated into
     * 
     * @param {any} code 2-Letter code for the language ( ex. 'EN' )
     */
    _.setTargetLanguage = function (code) {
        _.targetLang = code;
    }

    /**
     * Get a translation ( in a Promise object ) for the text supplied
     * 
     * @param {any} text Text to be translated
     * @returns {Promise} Object that will yield the translation
     */
    _.getTranslation = function (text) {
        _.searchText = text;

        return translate(text);
    }

    /**
     * @private
     * 
     * Translation method that fires the translation request at a custom built
     * Google Apps Script that uses Google Translator for the translations.
     * 
     * @param {any} text Text to be translated
     * @returns {Promise} Object that will yield the translation
     */
    var translate = function (text) {
        return new Promise((resolve) => {

            $.getJSON(url,
                {
                    source: _.sourceLang,
                    target: _.targetLang,
                    q: text
                },
                function (data) {
                    resolve(data);
                }
            );
        });
    }

    // Call the intialization function
    init();

    // Return the scope
    return _;
}