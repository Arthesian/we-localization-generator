var Main = function () {
    var _ = {};

    var languages = [];
    var autoTranslate = false;
    var translator;

    _.origJson = '';
    _.json = '';

    var init = function () {

        // Get the translator module if abailable
        if (Translator) {
            translator = new Translator();
        }

        // Bind events to elements on screen
        bind();
    }

    var bind = function () {

        // Bind the generate function to the 'Generate' button
        var btn = document.getElementById("btn_generate");
        btn.addEventListener("click", () => {

            if($("input:radio[name='existing_languages']").length && !$("input:radio[name='existing_languages']").is(":checked")) {
                alert('Please choose the default text for the newly generated localization properties'); return;
            }

            // Generate and merge new JSON
            _.json = getGeneratedAndMergedJSON();

            if (autoTranslate && translator) { 
                translateNewProperties();
            }

            setOutput();
        });

        // Bind the checkbox check
        var cb = document.getElementById("cb_translate");
        cb.addEventListener('change', (e) => {
            autoTranslate = !autoTranslate;
        });

        // Select all the text when an textarea is selected
        var textAreas = Array.prototype.slice.call(document.getElementsByTagName("textarea"));
        textAreas.forEach((area) => {
            area.addEventListener("click", () => {
                area.select();
            })
        });

        // Bind the events that trigger when input is detected
        var input = document.getElementById("input");
        input.addEventListener("change", () => {

            // Check if there is input
            var inputValue = input.value;
            if (!inputValue) {
                return;
            }

            // Parse the json for validity
            if (parseJson(inputValue)) {

                // Generate the language radio buttons to select default language ( if available in source )
                var languages;
                if (_.json.general.localization) {
                    languages = Object.keys(_.json.general.localization);
                }
                generateRadioButtonsForLanguages(languages);

                resetSettings();
            }

        });

        $(document).on('change', '[name="existing_languages"]', () => {
            if($("input:radio[name='existing_languages']:checked").val() === 'none') {
                $(cb).prop('checked', false);
                $(cb).css('display', 'none');
            }else{
                $(cb).css('display', 'inline-block');
            }
        });
    }

    var resetSettings = function(){
        $("#cb_translate").css('display', 'inline-block');
    }

    var setOutput = function () {
        // Put the output in the result textarea
        document.getElementById("result").value = JSON.stringify(_.json, null, 4);
    }

    var generateRadioButtonsForLanguages = function (langs) {
        var radiobuttonContainer = document.getElementById("language_radios");

        if (!langs) {
            radiobuttonContainer.classList.add("hidden");
            return;
        }

        var container = document.createElement("div");
        container.classList.add("language_radiobuttons");

        var title = document.createElement("h2");
        title.classList.add("setting_title");
        title.innerHTML = "Select default text";

        //var radioDefault = generateLanguageRadioButton("default");
        var radioNone = generateLanguageRadioButton("none");
        
        //container.appendChild(radioDefault);
        container.appendChild(radioNone);

        for (var lang of langs) {
            // generate radio buttons 
            var radio = generateLanguageRadioButton(lang);
            container.appendChild(radio);
        }

        radiobuttonContainer.innerHTML = "";
        radiobuttonContainer.classList.remove("hidden");
        radiobuttonContainer.appendChild(title);
        radiobuttonContainer.appendChild(container);
    }

    var generateLanguageRadioButton = function (lang) {

        var box = document.createElement("div");

        var label = document.createElement("label");
        label.setAttribute("for", lang);
        label.innerHTML = lang;

        var radio = document.createElement("input")
        radio.setAttribute("type", "radio");
        radio.setAttribute("id", lang);
        radio.setAttribute("name", "existing_languages");
        radio.setAttribute("value", lang);

        box.appendChild(radio);
        box.appendChild(label);

        return box;
    }

    var parseJson = function (input) {
        try {
            _.json = JSON.parse(input);
        } catch (e) {
            alert('The input project.json could not be parsed as valid JSON.');
            return;
        }

        if (!_.json.general) {
            alert('The input project.json did not contain the "general" property.');
            return;
        }

        if (!_.json.general.properties) {
            alert('The input project.json did not contain the "properties" node within the "general" property.');
            return;
        }

        if (!Object.keys(_.json.general.properties).length) {
            alert('The input project.json did not have any properties to generate translations for.');
            return;
        }

        _.origJson = JSON.parse(JSON.stringify(_.json));

        return _.json;
    }

    var getGeneratedAndMergedJSON = function () {

        var json = _.json;

        var origPproperties = getProperties(_.origJson);
        var origTranslations = getTranslations(_.origJson);
        var origLanguages = getLanguages(_.origJson)

        var generatedTranslations = generateTranslations(origPproperties, origLanguages);
        var newTranslations = mergeTranslations(origTranslations, generatedTranslations);

        if (!newTranslations) {
            delete json.general.localization;
        } else {
            json.general.localization = newTranslations;
        }

        return json;
    }

    var getProperties = function (json) {
        return json.general.properties;
    }

    var getTranslations = function (json) {
        return json.general.localization;
    }

    var getLanguages = function (json) {

        var languages = [];

        if (json.general.localization) {
            languages.push(...Object.keys(json.general.localization));
        }

        var string = document.getElementById("languages").value;

        if (!string && !languages.length) {
            alert("Please enter language codes for the translations");
            return;
        }

        if (string) {
            languages.push(...string.split(','));
        }

        // return array with unique values
        return [...new Set(languages)];
    }

    function generateTranslations(properties, languages) {

        // Start with new localization object
        var localization = {};

        // Set prefix
        var prefix = 'ui_';

        // Loop through the languages to generate
        for (var lang of languages) {

            // Get the lang code
            var langCode = lang.trim().toLowerCase();

            // Create new language object in Localization
            localization[langCode] = {};

            // Loop through all properties to generate localization items
            for (var key of Object.keys(properties)) {

                // Use existing language to generate localization items from
                var existing_lang = $("[name=existing_languages]:checked").val();

                // If it's NOT 'none', use it
                if (existing_lang && existing_lang !== 'none' && getTranslations(_.json)[existing_lang]) {
                    localization[langCode][prefix + key] = getTranslations(_.json)[existing_lang][prefix + key];
                }else{
                    // If it's set to 'none' create empty items
                    if(existing_lang && existing_lang === 'none'){
                        localization[langCode][prefix + key] = "";
                    }else{
                        // Default : create with key value
                        localization[langCode][prefix + key] = getProperties(_.origJson)[key].text;
                    }
                }

                // Also translate the options of combo lists
                if(properties[key].type === "combo") {

                    for(var i = 0; i < properties[key].options.length; i++) {
                        var comboKey = key + '_' + i;

                        if (existing_lang && existing_lang !== 'none' && getTranslations(_.json)[existing_lang]) {
                            localization[langCode][prefix + comboKey] = getTranslations(_.json)[existing_lang][prefix + comboKey];
                        }else{
                            if(existing_lang && existing_lang === 'none'){
                                localization[langCode][prefix + comboKey] = "";
                            }else{
                                localization[langCode][prefix + comboKey] = getProperties(_.origJson)[key].options[i].label;
                            }
                        }
                    }
                }
            }
        }

        var props = getProperties(_.json);

        // override old properties with new keys
        for (var key of Object.keys(properties)) {
            props[key].text = prefix + key;

            if(props[key].type === 'combo') {
                for(var i = 0; i < props[key].options.length; i++){
                    props[key].options[i].label = prefix + key + '_' + i;
                }
            }
        }

        return localization;
    }

    var getTranslation = function (text) {
        return translator.getTranslation(text);
    }

    var newLanguagePropertiesObject = {};
    var mergeTranslations = function (existing, generated) {

        // By default, save the generated as new language ( will be overridden afterwards )
        newLanguagePropertiesObject = generated;

        // If there was no existing language, just return the generated version ( no merge needed )
        if (!existing || !generated) return generated;

        // Save newly translated languages
        newLanguagePropertiesObject = {};

        // Loop through the languages in the generated array ( contains all )
        Object.keys(generated).forEach((langKey) => {
            var language = generated[langKey];

            // track new languages + keys
            newLanguagePropertiesObject[langKey] = {};

            // Loop through the translated properties of a certain language
            Object.keys(language).forEach((textKey) => {

                // If it already existed in the original file, take that translation - else use the generated one
                if (existing[langKey] && existing[langKey][textKey]) {
                    language[textKey] = existing[langKey][textKey];
                }else{

                    // Save this as a 'newly' translated property
                    newLanguagePropertiesObject[langKey][textKey] = language[textKey];
                }
            });
        });

        return generated;
    }

    var translateNewProperties = function() {

        var translationPromises = [];

        // Loop throught the lagnuages
        Object.keys(newLanguagePropertiesObject).forEach((langKey) => {

            translator.setTargetLanguage(langKey.split('-')[0]);

            // Loop through the keys within a language
            Object.keys(newLanguagePropertiesObject[langKey]).forEach(property => {
                
                // Get the text
                var text = newLanguagePropertiesObject[langKey][property];

                // Retrieve the translation for the text, and refresh the output
                var promise = getTranslation(text);

                translationPromises.push(promise);

                promise.then(translateObject => {
                    console.log(translateObject);
                    _.json.general.localization[langKey][property] = translateObject.translatedText;
    
                    setOutput();
                });

            });
        });

        Promise.all(translationPromises).then(values => {
            alert('Translations complete!');
        });
    }

    init();

    return _;
};